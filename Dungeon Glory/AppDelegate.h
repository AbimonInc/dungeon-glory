//
//  AppDelegate.h
//  Dungeon Glory
//
//  Created by Line Lajoie on 2013-12-09.
//  Copyright Abimon Inc. 2013. All rights reserved.
//

#import "cocos2d.h"

@interface Dungeon_GloryAppDelegate : NSObject <NSApplicationDelegate>
{
	NSWindow	*window_;
	CCGLView	*glView_;
}

@property (assign) IBOutlet NSWindow	*window;
@property (assign) IBOutlet CCGLView	*glView;

- (IBAction)toggleFullScreen:(id)sender;

@end
