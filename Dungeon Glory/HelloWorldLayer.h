//
//  HelloWorldLayer.h
//  Dungeon Glory
//
//  Created by Line Lajoie on 2013-12-09.
//  Copyright Abimon Inc. 2013. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
